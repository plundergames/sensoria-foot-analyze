export function roundWithDecimals(value: number, numberOfDecimals: number): number {
  const rounded = Math.round(Number(value + `e${numberOfDecimals}`)) + `e-${numberOfDecimals}`;
  return Number(Number(rounded).toLocaleString('en', {
    minimumFractionDigits: numberOfDecimals,
    maximumFractionDigits: numberOfDecimals
  }));
}

export function simpleRound(value: number, decimals: number): number {
  const factor = Math.pow(10, decimals);
  return Math.round(value * factor) / factor;
}
