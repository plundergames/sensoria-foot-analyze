import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import {NgMathPipesModule} from 'ngx-pipes';

import { RecordPageRoutingModule } from './record-routing.module';

import { RecordPage } from './record.page';
import { RecordDetailComponent } from './record-detail/record-detail.component';
import { MovingModePipe } from './record-detail/moving-mode.pipe';
import { LiveValueComponent } from './record-detail/live-value/live-value.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RecordPageRoutingModule,
    NgMathPipesModule
  ],
  exports: [
    MovingModePipe
  ],
  providers: [MovingModePipe],
  declarations: [RecordPage, RecordDetailComponent, MovingModePipe, LiveValueComponent]
})
export class RecordPageModule {}
