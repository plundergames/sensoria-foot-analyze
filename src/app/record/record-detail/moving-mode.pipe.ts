import {Pipe, PipeTransform} from '@angular/core';
import {MovingMode} from '../../store/record/record.model';

@Pipe({
  name: 'movingMode'
})
export class MovingModePipe implements PipeTransform {

  transform(value: MovingMode): string {
    switch (value) {
      case MovingMode.move:
        return 'Move';
      case MovingMode.stand:
        return 'Stand';
      case MovingMode.sit:
        return 'Sit';
      case MovingMode.relaxed:
        return 'Lie';
      case MovingMode.kneel:
        return 'Kneel';
      case MovingMode.all:
        return 'All';
    }
  }

}
