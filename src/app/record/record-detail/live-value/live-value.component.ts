import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {DeviceType} from '../../../store/ble/ble.model';
import {RecordDeviceEntry, RecordEntry} from '../../../store/record/record.model';

@Component({
  selector: 'app-live-value',
  templateUrl: './live-value.component.html',
  styleUrls: ['./live-value.component.scss']
})
export class LiveValueComponent implements OnInit, OnChanges {

  @Input()
  recordEntry: RecordEntry;

  @Input()
  displayFn: (recordDetail: RecordDeviceEntry) => string | number;

  values: Map<DeviceType, string | number>;

  deviceTypeEnum = DeviceType;

  constructor() {
  }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if ((changes.recordEntry || changes.displayFn) && this.displayFn) {
      const values = Object.entries(changes.recordEntry.currentValue as RecordEntry)
        .map(([deviceType, recordDetail]) => [deviceType, this.displayFn(recordDetail)] as [DeviceType, string | number])
        .sort(([a], [b]) => a < b ? -1 : 1);
      this.values = new Map(values);
    }
  }

}
