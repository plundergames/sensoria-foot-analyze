import {Component, OnInit} from '@angular/core';
import {UntilDestroy, untilDestroyed} from '@ngneat/until-destroy';
import {filter} from 'rxjs/operators';
import {simpleRound} from '../../functions/util';
import {RecordDeviceEntry, RecordEntry} from '../../store/record/record.model';
import {RecordQuery} from '../../store/record/record.query';
import {MovingModePipe} from './moving-mode.pipe';

@UntilDestroy()
@Component({
  selector: 'app-record-detail',
  templateUrl: './record-detail.component.html',
  styleUrls: ['./record-detail.component.scss']
})
export class RecordDetailComponent implements OnInit {
  recordEntry: RecordEntry;
  recordWithRelativePressure: RecordEntry;
  totalPressure: number;

  constructor(recordQuery: RecordQuery, private movingModePipe: MovingModePipe) {
    recordQuery.lastRecord$.pipe(
      filter(record => !!record),
      untilDestroyed(this),
    ).subscribe(record => {
      this.recordEntry = record;
      this.recordWithRelativePressure = this.getRelativePressure(record);
    });
  }

  getRelativePressure(record: RecordEntry): RecordEntry {
    this.totalPressure = Object.entries(record)
      .reduce((acc, [_, recordDevice]) => {
        const {
          frontLeft,
          frontRight,
          heel
        } = recordDevice.pressure;
        return acc + frontLeft + frontRight + heel;
      }, 0);

    const totalPressure = this.totalPressure || 1;
    return Object.entries(record)
      .reduce((acc, [key, value]) => {
        const {
          frontLeft,
          frontRight,
          heel
        } = value.pressure;
        acc[key] = {
          ...value,
          pressure: {
            heel: simpleRound(heel / totalPressure * 100, 1),
            frontRight: simpleRound(frontRight / totalPressure * 100, 1),
            frontLeft: simpleRound(frontLeft / totalPressure * 100, 1),
          }
        };
        return acc;
      }, {});

  }

  displayMovingModeFn = (recordDetail: RecordDeviceEntry): string => this.movingModePipe.transform(recordDetail.movingMode);
  displaySideMovementFn = (recordDetail: RecordDeviceEntry): number => recordDetail.movement.sideMovement;
  displaySideTiltMovementFn = (recordDetail: RecordDeviceEntry): number => recordDetail.movement.sideTiltMovement;

  displaySideTiltFn = (recordDetail: RecordDeviceEntry): number => recordDetail.sideTilt;
  displayFrontalTiltFn = (recordDetail: RecordDeviceEntry): number => recordDetail.frontalTilt;

  displayFrontRightPressureFn = (recordDetail: RecordDeviceEntry): string => recordDetail.pressure.frontRight + ' kg';
  displayFrontLeftPressureFn = (recordDetail: RecordDeviceEntry): string => recordDetail.pressure.frontLeft + ' kg';
  displayHeelPressureFn = (recordDetail: RecordDeviceEntry): string => recordDetail.pressure.heel + ' kg';

  displayFrontRightRelativePressureFn = (recordDetail: RecordDeviceEntry): string => recordDetail.pressure.frontRight + '%';
  displayFrontLeftRelativePressureFn = (recordDetail: RecordDeviceEntry): string => recordDetail.pressure.frontLeft + '%';
  displayHeelRelativePressureFn = (recordDetail: RecordDeviceEntry): string => recordDetail.pressure.heel + '%';

  ngOnInit(): void {
  }

}
