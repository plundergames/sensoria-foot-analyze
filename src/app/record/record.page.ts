import {Component, OnInit} from '@angular/core';
import {Actions} from '@datorama/akita-ng-effects';
import {Observable} from 'rxjs';
import {setReferencePressureActions, startRecordingAction, stopRecordingAction} from '../effects/record/record.actions';
import {BleQuery} from '../store/ble/ble.query';
import {RecordQuery} from '../store/record/record.query';

@Component({
  selector: 'app-record',
  templateUrl: './record.page.html',
  styleUrls: ['./record.page.scss'],
})
export class RecordPage implements OnInit {

  isRecording$: Observable<boolean>;

  isConnected$: Observable<boolean>;

  isCalibrated$: Observable<boolean>;

  constructor(private recordQuery: RecordQuery, private actions$: Actions, private bleQuery: BleQuery) {
    this.isRecording$ = recordQuery.isRecording$;
    this.isConnected$ = bleQuery.isConnected$;
    this.isCalibrated$ = recordQuery.isCalibrated$;
  }

  ngOnInit() {
  }

  startRecording() {
    this.actions$.dispatch(startRecordingAction);
    this.actions$.dispatch(setReferencePressureActions);
  }

  stopRecording() {
    this.actions$.dispatch(stopRecordingAction);
  }

  calibratePressure() {
    this.actions$.dispatch(setReferencePressureActions);
  }
}
