import {Injectable} from '@angular/core';
import {v4 as uuidv4} from 'uuid';
import {MovingMode, RecordState} from '../../store/record/record.model';
import {Result, ResultBase, ResultOverview} from '../../store/results/result.model';
import {EvaluationService} from '../evaluation/evaluation.service';

@Injectable({
  providedIn: 'root'
})
export class ResultsService {

  constructor() {
  }

  public createResult({violations, startDate, records}: RecordState): [ResultOverview, Result] {
    const base: ResultBase = {
      id: uuidv4(),
      startDate,
      endDate: new Date(),
    };

    const overview: ResultOverview = {
      ...base,
      violations: Object.values(violations).reduce((acc, item) => acc + (item || 0), 0),
      distribution: EvaluationService.smoothenRecords(records)
        .flatMap(record => Object.values(record))
        .reduce((acc, {movingMode}) => ({
          ...acc,
          [movingMode]: (acc[movingMode] || 0) + 1,
        }), {} as Partial<Record<MovingMode, number>>),
    };

    const result: Result = {
      ...base,
      records,
    };

    return [overview, result];
  }
}
