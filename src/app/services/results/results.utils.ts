import {MovingMode, RecordEntry} from '../../store/record/record.model';

export const getMovingMode = (record: RecordEntry): MovingMode => {
  const positions = Object.values(record).map(({movingMode}) => movingMode);
  return [MovingMode.move, MovingMode.stand, MovingMode.sit, MovingMode.relaxed, MovingMode.kneel]
    .find(movingMode => positions.includes(movingMode));
};

export const getPositions = (records: RecordEntry[]): MovingMode[] => records.map(getMovingMode);
