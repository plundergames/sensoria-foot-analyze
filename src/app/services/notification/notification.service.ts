import {Injectable} from '@angular/core';
import {LocalNotifications} from '@ionic-native/local-notifications/ngx';
import {UntilDestroy} from '@ngneat/until-destroy';
import {MovingMode, RecordEntry} from '../../store/record/record.model';
import {postureCheckInterval, RecordQuery} from '../../store/record/record.query';

const threshold = postureCheckInterval * 0.8;

@UntilDestroy()
@Injectable({
  providedIn: 'root'
})
export class NotificationService {


  constructor(recordQuery: RecordQuery, private localNotifications: LocalNotifications) {
  }

  private static hasCondition(records: RecordEntry[], conditionFn: (RecordDeviceEntry) => boolean, needsEvery = false): boolean {
    return records
      .filter(({right, left}) => [right, left].filter(detail => detail).every(conditionFn))
      .length >= (needsEvery ? records.length : threshold);
  }

  getBadPostures(records: RecordEntry[]): string[] {
    const isSitting = NotificationService.hasCondition(records, ({movingMode}) => movingMode === MovingMode.sit, true);
    const isMovingSide = NotificationService.hasCondition(records, ({movement: {sideMovement}}) => sideMovement > 8);
    const isMovingSideTilt = NotificationService.hasCondition(records, ({movement: {sideTiltMovement}}) => sideTiltMovement > 12);
    const isSideTilt = NotificationService.hasCondition(records, ({sideTilt}) => sideTilt > 5 || sideTilt <= -15);
    const isFrontalTilt = NotificationService.hasCondition(records, ({frontalTilt}) => frontalTilt > 4);
    return isSitting
      ? [
        isMovingSide ? 'Side movement detected!' : null,
        isMovingSideTilt ? 'Side tilt movement detected!' : null,
        isSideTilt ? 'Side tilt detected!' : null,
        isFrontalTilt ? 'Frontal tilt detected!' : null,
      ].filter(message => message)
      : [];
  }

  showNotification(title: string, messages: string[]) {
    console.log(messages);
    this.localNotifications.schedule({
      id: 1,
      title,
      text: messages.join('\n'),
      silent: false,
      vibrate: true,
    });
  }

  getPositionTooLong(positions: MovingMode[]): string | null {
    const twoMinEntries = 2 * 2 * 60;
    const tenSecondsEntries = 2 * 10;
    const hourlyEntries = 2 * 60 * 60;
    const twentyMinEntries = 2 * 20 * 60;
    const lastHour = positions.slice(-hourlyEntries);
    const lastTwentyMin = positions.slice(-twentyMinEntries);
    const lastTenSeconds = positions.slice(-tenSecondsEntries);

    if (lastHour.length === hourlyEntries
      && lastHour.filter(movingMode => [MovingMode.sit, MovingMode.relaxed].includes(movingMode)).length >= hourlyEntries - twoMinEntries) {
      return 'Sitting too long (1h)! Please stand up!';
    }

    if (lastTwentyMin.length === twentyMinEntries
      && lastTenSeconds.filter(movingMode => movingMode === MovingMode.stand).length === tenSecondsEntries
      && lastTwentyMin.filter(movingMode => movingMode === MovingMode.stand).length >= twentyMinEntries - twoMinEntries) {
      return 'Standing too long (20 min)! Please sit or move!';
    }
    return null;
  }
}
