import {Injectable} from '@angular/core';
import {DeviceType} from '../../store/ble/ble.model';
import {RecordDeviceEntry, RecordEntry} from '../../store/record/record.model';

@Injectable({
  providedIn: 'root'
})
export class CompressService {

  constructor() {
  }

  compressEntries(entries: RecordEntry[]): [DeviceType, number[]][][] {
    return entries
      .map(entry => Object.entries(entry)
        .map(([deviceType, deviceEntry]: [DeviceType, RecordDeviceEntry]) =>
          [deviceType, this.compress(deviceEntry)]));
  }

  compress({
             movingMode,
             movement: {
               frontalMovement,
               sideMovement,
               sideTiltMovement,
             },
             frontalTilt,
             sideTilt,
             pressure: {
               frontLeft,
               frontRight,
               heel,
             },
           }: RecordDeviceEntry): number[] {
    return [
      movingMode,
      frontalMovement,
      sideMovement,
      sideTiltMovement,
      frontalTilt,
      sideTilt,
      frontLeft,
      frontRight,
      heel];
  }

  decompressEntries(compressed: [DeviceType, number[]][][]): RecordEntry[] {
    return compressed
      .map(entry => entry.reduce((acc, [deviceType, data]) => ({
          ...acc,
          [deviceType]: this.decompress(data),
        }),
        {} as RecordEntry)
      );
  }

  decompress([
               movingMode,
               frontalMovement,
               sideMovement,
               sideTiltMovement,
               frontalTilt,
               sideTilt,
               frontLeft,
               frontRight,
               heel,
             ]: number[]): RecordDeviceEntry {
    return {
      movingMode,
      movement: {
        frontalMovement,
        sideMovement,
        sideTiltMovement,
      },
      frontalTilt,
      sideTilt,
      pressure: {
        frontLeft,
        frontRight,
        heel,
      },
    };
  }
}
