/* eslint-disable no-console */
import {Injectable, NgZone} from '@angular/core';
import {Actions} from '@datorama/akita-ng-effects';
import {BLE} from '@ionic-native/ble/ngx';
import {UntilDestroy, untilDestroyed} from '@ngneat/until-destroy';
import {map} from 'rxjs/operators';
import {emitLiveDataAction, tryLiveDataAction} from '../../effects/ble/ble.actions';
import {Device, DeviceType} from '../../store/ble/ble.model';
import {BleQuery} from '../../store/ble/ble.query';
import {BleStore} from '../../store/ble/ble.store';
import {DataConverterService} from '../data-converter/data-converter.service';
import {deviceKey} from '../storage/storage.const';
import {StorageService} from '../storage/storage.service';
import {dataCharacteristicsId, dataServiceId} from './ble.const';
import {groupInt2} from './ble.utils';

@UntilDestroy()
@Injectable({
  providedIn: 'root'
})
export class BleService {
  constructor(private ble: BLE,
              private storageService: StorageService,
              private ngZone: NgZone,
              private bleStore: BleStore,
              private bleQuery: BleQuery,
              private actions$: Actions) {
  }

  async tryConnect(deviceType: DeviceType) {
    const isEnabled = await this.ble.isEnabled().then(() => true).catch(() => false);
    this.bleStore.update(state => ({...state, bluetoothEnabled: isEnabled}));

    const {[deviceType]: savedDevice} = await this.storageService.get(deviceKey) || {};
    const isConnected = savedDevice && (await this.ble.isConnected(savedDevice.id).catch(() => false));
    const isScanning = !!this.bleQuery.scanningType();

    if (isEnabled && !isScanning) {
      if (!savedDevice) {
        this.bleStore.update({scanningType: deviceType});
        this.ble.startScan([])
          .pipe(untilDestroyed(this))
          .subscribe(device => this.onDeviceFound(device));
      } else {
        if (isConnected) {
          this.bleStore.update(store => ({
            ...store,
            connectedDevices: {
              ...store.connectedDevices,
              [deviceType]: savedDevice
            }
          }));
        } else {
          this.connect(savedDevice, deviceType);
        }
      }
    }
  }

  onDeviceFound(device: any) {
    this.ngZone.run(() => {
      if (device) {
        if (device?.name?.match(/Sensoria/i)) {
          this.bleStore.update(store => ({...store, scannedDevices: [...store.scannedDevices, device]}));
        }
      }
    });
  }

  connect(device: Device, deviceType: DeviceType) {
    this.bleStore.update(store => ({...store, waitingForConnect: {...store.waitingForConnect, [deviceType]: device}}));
    this.ble.connect(device.id)
      .pipe(untilDestroyed(this))
      .subscribe(
        async data => this.onConnect(data, deviceType),
        () => this.onDisconnect(deviceType));
  }

  onDisconnect(deviceType: DeviceType) {
    this.ngZone.run(() => {
      console.info(`${deviceType}-type could not connect or disconnected!`);
      this.bleStore.update(store => ({
        ...store,
        connectedDevices: {
          ...store.connectedDevices,
          [deviceType]: null
        },
        waitingForConnect: {...store.waitingForConnect, [deviceType]: null}
      }));
    });
  }

  stopScanning() {
    this.ble.stopScan()
      .then(() => console.info('Scan stopped!'))
      .catch(e => console.info('Scan could not stopped!', e));
    this.bleStore.sUpdate({scanningType: null, scannedDevices: []});
  }

  async onConnect(data: any, deviceType: DeviceType) {
    console.info('Connected', deviceType, data);
    this.ngZone.run(() => {
      this.storageService.partialUpdate(deviceKey, {[deviceType]: data});
      this.bleStore.update(store => ({
        ...store,
        connectedDevices: {...store.connectedDevices, [deviceType]: data},
        waitingForConnect: {...store.waitingForConnect, [deviceType]: null}
      }));
      this.actions$.dispatch(tryLiveDataAction);
    });
  }

  startLiveData(deviceType: DeviceType, deviceId: string) {
    if (deviceId) {
      this.ble.startNotification(
        deviceId,
        dataServiceId,
        dataCharacteristicsId)
        .pipe(
          map(buffer => DataConverterService.arrayBufferToIntArray(buffer[0])),
          map(list => list.flatMap(item => DataConverterService.intToBitInt(item, 4))),
          map(list => groupInt2(list)))
        .subscribe(data => {
          this.ngZone.run(() => {
            this.actions$.dispatch(emitLiveDataAction({liveData: {deviceType, data}}));
          });
        }, err => console.warn(err));
    }
  }

  async stopLiveData(deviceId: string): Promise<void> {
    if (deviceId) {
      await this.ble.stopNotification(
        deviceId,
        dataServiceId,
        dataCharacteristicsId).catch(() => null);
    }
  }

  async disconnect(id: string): Promise<void> {
    if (id) {
      await this.ble.disconnect(id);
    }
  }
}
