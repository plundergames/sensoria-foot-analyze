import {Injectable, NgZone} from '@angular/core';
import {BLE} from '@ionic-native/ble/ngx';
import {UntilDestroy, untilDestroyed} from '@ngneat/until-destroy';
import {BehaviorSubject, combineLatest} from 'rxjs';
import {map, scan, startWith} from 'rxjs/operators';
import {DeviceType} from '../../store/ble/ble.model';
import {BleQuery} from '../../store/ble/ble.query';
import {DataConverterService} from '../data-converter/data-converter.service';
import {groupInt2, setRange} from './ble.utils';

@UntilDestroy()
@Injectable({
  providedIn: 'root'
})
export class BleDebugService {

  public debugNotifications$ = new BehaviorSubject([]);

  constructor(private ble: BLE, private ngZone: NgZone, private bleQuery: BleQuery) {
  }

  startDebugNotifications(data: any) {
    const notifications = data.characteristics
      .filter(c => c.properties.includes('Notify'))
      .map((c) => this.ble.startNotification(data.id, c.service, c.characteristic)
        .pipe(
          map(buffer => DataConverterService.arrayBufferToIntArray(buffer[0])),
          map(list => list.flatMap(item => DataConverterService.intToBitInt(item, 4))),
          map(list => groupInt2(list)),
          map(list => list.map(item => ({value: item}))),
          scan((acc, list) => setRange(acc, list)),
          startWith(null),
          map(notificationData => ({
              data: notificationData,
              id: c.characteristic,
            }),
          )
        ));

    combineLatest(notifications)
      .pipe(untilDestroyed(this))
      .subscribe(n => {
        this.ngZone.run(() => {
          this.debugNotifications$.next(n);
        });
      });
  }

  stopDebugNotifications() {
    const device = this.bleQuery.connectedDevice(DeviceType.right);
    device?.characteristics
      ?.filter(c => c.properties.includes('Notify'))
      .forEach(c => this.ble.stopNotification(device.id, c.service, c.characteristic));
  }
}
