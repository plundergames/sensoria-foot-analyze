import {DataConverterService} from '../data-converter/data-converter.service';

export const groupInt2 = (int2Array) =>
  [...Array(Math.ceil(int2Array.length / 5))
    .keys()]
    .filter(value => value >= 4) // first values are garbage
    .map(value => value * 5)
    .map(value => int2Array.slice(value, value + 5))
    .map(array => DataConverterService.intArrayToNumber(array, Math.pow(2, 2)))
    .map((value, index) => {
      if (index < 3) {
        return value;
      }
      return value >= 512 ? (value - 512 * 2) : (value);
    });

export const setRange = (acc, list) =>
  acc.map((accItem, index) => {
    const listItemValue = list[index].value;
    return {
      value: listItemValue,
      min: accItem.min != null && accItem.min < listItemValue ? accItem.min : listItemValue,
      max: accItem.max != null && accItem.max > listItemValue ? accItem.max : listItemValue
    };
  });
