export enum DataTypes {
  uint8 = 1,
  uint16 = 2,
  uint32 = 4,
}
