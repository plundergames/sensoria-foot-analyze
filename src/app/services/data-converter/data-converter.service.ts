import {Injectable} from '@angular/core';
import {DataTypes} from './data-converter.model';

@Injectable({
  providedIn: 'root',
})
export class DataConverterService {

  constructor() {
  }

  static arrayBufferToInt(arrayBuffer: any, offset: number, dataType: DataTypes): number {
    const dataView = new DataView(arrayBuffer);
    switch (dataType) {
      case  DataTypes.uint8:
        return dataView.getUint8(offset);
      case DataTypes.uint16:
        return dataView.getUint16(offset, true);
      case DataTypes.uint32:
        return dataView.getUint32(offset, true);
      default:
        return -1;
    }
  }

  static arrayBufferToSignedIntArray(arrayBuffer: ArrayBuffer): number[] {
    console.log(arrayBuffer.byteLength);
    return Array.from(new Int8Array(arrayBuffer));
  }

  static arrayBufferToIntArray(arrayBuffer: ArrayBuffer): number[] {
    return Array.from(new Uint8Array(arrayBuffer));
  }

  static arrayBufferToString(arrayBuffer: ArrayBuffer): string {
    return String.fromCharCode(...DataConverterService.arrayBufferToIntArray(arrayBuffer));
  }

  static int8ToInt4(int8: number): number[] {
    return [Math.floor(int8 / 16), int8 % 16];
  }

  static intToBitInt(int8: number, splits: number): number[] {
    const toBits = (rest, divider) => {
      const newRest = Math.floor(rest / divider);
      const newValue = rest % divider;

      return newRest > 0
        ? [...toBits(newRest, divider), newValue]
        : [newValue];
    };

    return [...new Array(splits).fill(0), ...toBits(int8, Math.pow(256, 1 / splits))].slice(-splits);
  }

  static intToArrayBuffer(intValue: number, dataType: DataTypes): ArrayBuffer {
    const arrayBuffer = new ArrayBuffer(dataType);
    const dataView = new DataView(arrayBuffer);
    switch (dataType) {
      case  DataTypes.uint8:
        dataView.setUint8(0, intValue);
        break;
      case DataTypes.uint16:
        dataView.setUint16(0, intValue, true);
        break;
      case DataTypes.uint32:
        dataView.setUint32(0, intValue, true);
        break;
    }
    return arrayBuffer;
  }

  static stringToArrayBuffer(stringValue: string): ArrayBuffer {
    const charCodeArray = Array.from(stringValue)
      .map(char => char.charCodeAt(0));
    return new Uint8Array(charCodeArray).buffer as ArrayBuffer;
  }

  static appendArrayBuffers(...arrayBuffers: ArrayBuffer[]): ArrayBuffer {
    return arrayBuffers.reduce((acc, arrayBuffer) => {
      const tmp = new Uint8Array(arrayBuffer.byteLength + acc.byteLength);
      tmp.set(new Uint8Array(acc), 0);
      tmp.set(new Uint8Array(arrayBuffer), acc.byteLength);
      return tmp.buffer as ArrayBuffer;
    }, new ArrayBuffer(0));
  }

  static cutTrailingZeros(arrayBuffer: ArrayBuffer): ArrayBuffer {
    const firstZeroPosition = new Uint8Array(arrayBuffer)
      .findIndex(charCode => charCode === 0);
    return firstZeroPosition >= 0
      ? arrayBuffer.slice(0, firstZeroPosition)
      : arrayBuffer;
  }

  static intArrayToNumber(array: number[], maxItemValue: number): number {
    return array.reduce((acc, item, index) => acc + item * Math.pow(maxItemValue, array.length - index - 1), 0);
  }
}
