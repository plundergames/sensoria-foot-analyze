import {TestBed} from '@angular/core/testing';
import {DataTypes} from './data-converter.model';

import {DataConverterService} from './data-converter.service';

describe('DataConverterService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  describe('arrayBufferToInt', () => {
    it('uint8', () => {
      const arrayBuffer = new Uint8Array([ 1, 2, 3 ]).buffer;
      const result = DataConverterService.arrayBufferToInt(arrayBuffer, 1, DataTypes.uint8);
      expect(result).toBe(2);
    });

    it('uint16', () => {
      const arrayBuffer = new Uint8Array([ 1, 2, 3, 4 ]).buffer;
      const result = DataConverterService.arrayBufferToInt(arrayBuffer, 1, DataTypes.uint16);
      expect(result).toBe(3 * 256 + 2);
    });

    it('uint32', () => {
      const arrayBuffer = new Uint8Array([ 1, 2, 3, 4, 5 ]).buffer;
      const result = DataConverterService.arrayBufferToInt(arrayBuffer, 1, DataTypes.uint32);
      expect(result).toBe(5 * Math.pow(256, 3)
        + 4 * Math.pow(256, 2)
        + 3 * 256
        + 2);
    });

    it('no type', () => {
      const arrayBuffer = new Uint8Array([ 1, 2, 3 ]).buffer;
      const result = DataConverterService.arrayBufferToInt(arrayBuffer, 1, null);
      expect(result).toBe(-1);
    });
  });

  describe('intToArrayBuffer', () => {
    it('uint8', () => {
      const result = DataConverterService.intToArrayBuffer(12, DataTypes.uint8);
      expect(new Uint8Array(result)).toEqual(new Uint8Array([ 12 ]));
    });

    it('uint16', () => {
      const result = DataConverterService.intToArrayBuffer(4 * 256
        + 3
        , DataTypes.uint16);
      expect(new Uint8Array(result)).toEqual(new Uint8Array([ 3, 4 ]));
    });

    it('uint32', () => {
      const result = DataConverterService.intToArrayBuffer(
        5 * Math.pow(256, 3)
        + 4 * Math.pow(256, 2)
        + 3 * 256
        + 2
        , DataTypes.uint32);
      expect(new Uint8Array(result)).toEqual(new Uint8Array([ 2, 3, 4, 5 ]));
    });

    it('no type', () => {
      const result = DataConverterService.intToArrayBuffer(12, null);
      expect(new Uint8Array(result)).toEqual(new Uint8Array([]));
    });

    it('int value is bigger than the datatype can handle', () => {
      const result = DataConverterService.intToArrayBuffer(257, DataTypes.uint8);
      expect(new Uint8Array(result)).toEqual(new Uint8Array([ 1 ]));
    });
  });

  it('appendArrayBuffers', () => {
    const buff1 = new Uint8Array([ 1, 2, 3 ]).buffer as ArrayBuffer;
    const buff2 = new Uint8Array([ 4, 5, 6 ]).buffer as ArrayBuffer;
    const buff3 = new Uint8Array([ 7, 8, 9 ]).buffer as ArrayBuffer;

    const result = DataConverterService.appendArrayBuffers(buff1, buff2, buff3);
    expect(new Uint8Array(result)).toEqual(new Uint8Array([ 1, 2, 3, 4, 5, 6, 7, 8, 9 ]));
  });

  it('stringToArrayBuffer', () => {
    const stringValue = 'dummyString';
    const result = DataConverterService.stringToArrayBuffer(stringValue);
    expect(new Uint8Array(result)).toEqual(new Uint8Array([ 100, 117, 109, 109, 121, 83, 116, 114, 105, 110, 103 ]));
  });

  it('arrayBufferToString', () => {
    const arrayBuffer = new Uint8Array([ 100, 117, 109, 109, 121, 83, 116, 114, 105, 110, 103 ]).buffer as ArrayBuffer;
    const result = DataConverterService.arrayBufferToString(arrayBuffer);
    expect(result).toEqual('dummyString');
  });

  describe('cutTrailingZeros', () => {
    it('cut trailing zeros when it has trailing zeros', () => {
      const arrayBuffer = new Uint8Array([ 1, 2, 3, 0, 0, 0 ]).buffer as ArrayBuffer;
      const result = DataConverterService.cutTrailingZeros(arrayBuffer);
      expect(new Uint8Array(result)).toEqual(new Uint8Array([ 1, 2, 3 ]));
    });

    it('return original buffer, when no trailing zeros', () => {
      const arrayBuffer = new Uint8Array([ 1, 2, 3 ]).buffer as ArrayBuffer;
      const result = DataConverterService.cutTrailingZeros(arrayBuffer);
      expect(new Uint8Array(result)).toEqual(new Uint8Array([ 1, 2, 3 ]));
    });
  });
});
