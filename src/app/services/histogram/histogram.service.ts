import {Injectable} from '@angular/core';
import {DeviceType} from '../../store/ble/ble.model';
import {Bucket, Distribution, Histogram} from './histogram.model';

@Injectable({
  providedIn: 'root'
})
export class HistogramService {

  constructor() {
  }

  public static createDataset(data: number[], bucketThreshold: number[]) {
    const emptyBuckets: Bucket[] = [...bucketThreshold, null]
      .reduce(([lastItem, acc], item) => [item, [...acc, [lastItem, item]]], [null, [] as any])[1]
      .map(([min, max]) => ({
        min,
        max,
        count: 0,
      }));

    const buckets = data.reduce((acc, item) => {
      const bucket = acc.find(({min, max}) =>
        (item >= min || min === null)
        && (item <= max || max === null));
      bucket.count++;
      return acc;
    }, emptyBuckets);

    return {
      buckets,
      max: Math.max(...data),
      min: Math.min(...data),
      avg: data.reduce((acc, item) => acc + item, 0) / (data.length || 1),
    };
  }

  createHistogram(data: [DeviceType, number[]][], bucketThreshold: number[]): Histogram {
    return data.map(([deviceType, records]) => [
      deviceType,
      HistogramService.createDataset(records, bucketThreshold),
    ]);
  }

  createDistribution(data: Record<string, [DeviceType, number[]][]>): Distribution {
    const temp = Object.entries(data)
      .flatMap(([name, entry]) =>
        entry.map(([deviceType, itemData]) => [deviceType, name, itemData] as [DeviceType, string, number[]]))
      .reduce((acc, [deviceType, name, itemData]) => ({
        ...acc,
        [deviceType]: [...(acc[deviceType] || []), [name, itemData]]
      }), {} as Partial<Record<DeviceType, [string, number[]][]>>);

    return Object.entries(temp)
      .map(([deviceType, distributionValues]) => [
        deviceType as DeviceType,
        distributionValues.map(([name, dataList]) =>
          ({
            name,
            count: dataList.reduce((acc, item) => acc + item, 0)
          }))]);
  }
}
