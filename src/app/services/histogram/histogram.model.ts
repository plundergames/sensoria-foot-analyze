export type Histogram = [string, HistogramDataSet][];

export interface HistogramDataSet {
  min: number;
  max: number;
  avg: number;
  buckets: Bucket[];
}

export interface Bucket {
  min: number;
  max: number;
  count: number;
}

export type Distribution = [string, DistributionEntity[]][];

export interface DistributionEntity {
  name: string;
  count: number;
}
