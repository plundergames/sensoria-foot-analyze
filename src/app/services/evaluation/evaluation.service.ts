/* eslint-disable @typescript-eslint/naming-convention,quote-props */
import {Injectable} from '@angular/core';
import {DeviceType} from '../../store/ble/ble.model';
import {MovingMode, RecordDeviceEntry, RecordEntry} from '../../store/record/record.model';
import {Result, ResultDetail} from '../../store/results/result.model';
import {HistogramService} from '../histogram/histogram.service';
import {TimeDataService} from '../time-data/time-data.service';

@Injectable({
  providedIn: 'root'
})
export class EvaluationService {

  constructor(private histogramService: HistogramService, private timeDataService: TimeDataService) {
  }

  public static smoothenRecords(records: RecordEntry[]): RecordEntry[] {
    return records
      .reduce((acc, item) => {
        const lastCluster = acc[acc.length - 1];
        if (lastCluster && Object.values(lastCluster[0])[0].movingMode === Object.values(item)[0].movingMode) {
          lastCluster.push(item);
        } else {
          acc.push([item]);
        }
        return acc;
      }, [] as RecordEntry[][])
      .filter(cluster => cluster.length >= 4) // over 2s the same movement
      .flat(1);
  }

  private static getSpecificValues(recordsByDevice: Partial<Record<DeviceType, RecordDeviceEntry[]>>,
                                   mapFn: (RecordDeviceEntry) => number): [DeviceType, number[]][] {
    return Object.entries(recordsByDevice).map(([deviceType, records]) => [deviceType as DeviceType, records.map(mapFn)]);
  }


  evaluateResult({id, endDate, startDate, records}: Result, movingMode: MovingMode): ResultDetail {
    const filteredRecords = movingMode !== MovingMode.all
      ? EvaluationService.smoothenRecords(records)
        .filter(record => Object.values(record)[0].movingMode === movingMode)
      : records;

    const recordsByDevice = filteredRecords
      .flatMap(record => Object.entries(record) as [DeviceType, RecordDeviceEntry][])
      .reduce((acc, [deviceType, data]) => {
        const deviceRecords = acc[deviceType] || [];
        deviceRecords.push(data);
        acc[deviceType] = deviceRecords;
        return acc;
      }, {} as Record<DeviceType, RecordDeviceEntry[]>);

    const totalPressures = EvaluationService.getSpecificValues(recordsByDevice, ({
                                                                                   pressure: {
                                                                                     heel,
                                                                                     frontLeft,
                                                                                     frontRight
                                                                                   }
                                                                                 }) => heel + frontLeft + frontRight);

    return {
      totalRecords: filteredRecords.length,
      id,
      endDate,
      startDate,
      frontalTilt: this.histogramService.createHistogram(
        EvaluationService.getSpecificValues(recordsByDevice, r => r.frontalTilt),
        [-40, -20, -10, -5, 0, 5, 10, 20, 40]),
      sideTilt: this.histogramService.createHistogram(
        EvaluationService.getSpecificValues(recordsByDevice, r => r.sideTilt),
        [-40, -25, -15, -10, -5, 0, 5, 10, 20, 40]),
      pressure: this.histogramService.createDistribution({
        'Front Right': EvaluationService.getSpecificValues(recordsByDevice, r => r.pressure.frontRight),
        'Front Left': EvaluationService.getSpecificValues(recordsByDevice, r => r.pressure.frontLeft),
        'Heel': EvaluationService.getSpecificValues(recordsByDevice, r => r.pressure.heel),
      }),
      sideMovement: this.histogramService.createHistogram(
        EvaluationService.getSpecificValues(recordsByDevice, r => r.movement.sideMovement),
        [8, 15, 30, 50, 100]),
      sideTiltMovement: this.histogramService.createHistogram(
        EvaluationService.getSpecificValues(recordsByDevice, r => r.movement.sideTiltMovement),
        [12, 25, 50, 100]),
      totalPressure: this.histogramService.createHistogram(totalPressures,
        [2, 4, 6, 8, 10, 13, 17, 21, 25, 30]
      ),
      pressureOverTime: this.timeDataService.getTimeData(totalPressures, startDate, endDate),
    };
  }
}
