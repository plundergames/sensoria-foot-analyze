import {Injectable} from '@angular/core';
import {DeviceType} from '../../store/ble/ble.model';
import {TimeData} from './time-data.model';

@Injectable({
  providedIn: 'root'
})
export class TimeDataService {

  constructor() {
  }

  getTimeData(data: [DeviceType, number[]][], startDate: Date, endDate: Date): TimeData {
    const startTime = startDate.getTime();
    const endTime = endDate.getTime();
    const diff = endTime - startTime;
    const reducedData = data.map(([deviceType, values]) => {
      const bucketSize = Math.ceil(values.length / 100);
      return [deviceType,
        values.reduce((acc, item, index) => {
          const reducedIndex = Math.floor(index / bucketSize);
          const existingBucket = acc[reducedIndex] || [];
          existingBucket.push(item);
          acc[reducedIndex] = existingBucket;
          return acc;
        }, Array.from({length: Math.min(100, values.length)}) as number[][])
          .filter(bucket => bucket)
          .map(bucket => bucket.reduce((acc, item) => acc + item, 0) / (bucket.length || 0))
      ] as [DeviceType, number[]];
    });
    const entries = reducedData.reduce((acc, [_, values]) => values.length > acc ? values.length : acc, 0);
    const diffEntries = diff / Math.max(entries - 1, 1);
    const timeEntries = Array.from({length: entries}, (_, i) => i)
      .map(index => Math.round(startTime + index * diffEntries))
      .map(timestamp => new Date(timestamp));

    return {
      data: reducedData,
      timeEntries,
    };
  }
}
