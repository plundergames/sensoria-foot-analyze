import {DeviceType} from '../../store/ble/ble.model';

export interface TimeData {
  data:  [DeviceType, number[]][];
  timeEntries: Date[];
}
