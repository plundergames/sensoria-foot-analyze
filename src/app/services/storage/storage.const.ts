export const recordsKey = 'currentRecords';
export const deviceKey = 'currentDevice';
export const resultsOverviewKey = 'resultsOverview';
export const startDateKey = 'startDate';
export const currentViolationsKey = 'violations';
export const settingsKey = 'settings';
