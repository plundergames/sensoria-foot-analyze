import {Injectable} from '@angular/core';

import {Storage} from '@ionic/storage-angular';

@Injectable({
  providedIn: 'root'
})
export class StorageService {
  private myStorage: Storage | null = null;

  constructor(private storage: Storage) {
  }

  public async set(key: string, value: any): Promise<void> {
    await this.getStorage().then(s => s?.set(key, value));
  }

  public async partialUpdate(key: string, value: any): Promise<void> {
    const storage = await this.getStorage();
    const existing = (await storage.get(key)) || {};
    await storage.set(key, {...existing, ...value});
  }

  public async add(key: string, value: any) {
    const storage = await this.getStorage();
    const existing = (await storage.get(key)) || [];
    await storage.set(key, [...existing, value]);
  }

  public async remove(key: string, id: string) {
    const storage = await this.getStorage();
    const existing = (await storage.get(key)) || [];
    await storage.set(key, existing.filter(item => item.id !== id));
  }

  public async get(key: string): Promise<any> {
    return this.getStorage()
      .then(s => s.get(key))
      .catch(() => null);
  }

  public async clear(key: string): Promise<any> {
    return this.getStorage()
      .then(s => s.remove(key))
      .catch(() => null);
  }

  public async clearProperty(key: string, property: string): Promise<void> {
    const storage = await this.getStorage();
    const existing = await storage.get(key);
    const {[property]: _, ...newObject} = existing;
    await storage.set(key, newObject);
  }

  private async getStorage(): Promise<Storage> {
    if (!this.myStorage) {
      this.myStorage = await this.storage.create();
    }
    return this.myStorage;
  }

}
