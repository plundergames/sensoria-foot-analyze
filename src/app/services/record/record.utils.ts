import {simpleRound} from '../../functions/util';
import {DeviceType} from '../../store/ble/ble.model';
import {Pressure} from '../../store/record/record.model';

export function pressureToKg(value: number, valueKg0: number, diff20: number) {
  const diffValue = Math.max(valueKg0 - value, 0);
  const resultKg = diffValue / diff20 * 20;
  return resultKg > 1
    ? simpleRound(resultKg, 1)
    : 0;
}

export function frontalTiltToDegree(value: number, deviceType: DeviceType) {
  return deviceType === DeviceType.left
    ? value * 2
    : value * -2;
}

export function sideTiltToDegree(value: number) {
  return value * 2;
}

export function getDiff(array: number[]): number {
  return Math.max(...array) - Math.min(...array);
}

export function getExtremeValues(array: number[][]): number[] {
  return array[0].map((_, index) =>
    array
      .map(data => data[index])
      .reduce((acc, item) =>
        Math.abs(item) > Math.abs(acc) ? item : acc, 0));
}

export function subtractPressure(press1: Pressure, press2: Pressure): Pressure {
  return {
    frontLeft: press1.frontLeft - press2.frontLeft,
    frontRight: press1.frontRight - press2.frontRight,
    heel: press1.heel - press2.heel,
  };
}
