import {Injectable} from '@angular/core';
import {DeviceType} from '../../store/ble/ble.model';
import {Movement, MovingMode, RecordDeviceEntry, RecordEntry} from '../../store/record/record.model';
import {RecordQuery} from '../../store/record/record.query';
import {frontalTiltToDegree, getDiff, getExtremeValues, pressureToKg, sideTiltToDegree} from './record.utils';

@Injectable({
  providedIn: 'root'
})
export class RecordService {

  constructor(private recordQuery: RecordQuery) {
  }

  public static setMovingMode(record: RecordEntry) {
    const totalPressure = Object.values(record)
      .map(({
              pressure: {
                frontRight,
                frontLeft,
                heel
              }
            }) => frontLeft + frontRight + heel)
      .reduce((acc, item) => acc + item, 0);
    const modes = Object.values(record).map(r => RecordService.analyzeMovingMode(r, totalPressure));
    const dominantMode = [MovingMode.move, MovingMode.stand, MovingMode.sit, MovingMode.relaxed, MovingMode.kneel]
      .find(movingMode => modes.includes(movingMode));
    Object.values(record).forEach(r => r.movingMode = dominantMode);
    return record;
  }

  private static analyzeMovingMode({
                                     frontalTilt,
                                     sideTilt,
                                     movement: {frontalMovement}
                                   }: RecordDeviceEntry, totalPressure: number): MovingMode {
    if (frontalMovement > 50) {
      return MovingMode.move;
    } else if (totalPressure >= 40) {
      return MovingMode.stand;
    } else if (frontalTilt + sideTilt < -62 || frontalTilt < -45) {
      return MovingMode.relaxed;
    } else if (frontalTilt + sideTilt > 80) {
      return MovingMode.kneel;
    } else {
      return MovingMode.sit;
    }
  }

  private static analyzeMovement(records: any[]): Movement {
    return {
      frontalMovement: getDiff(records.map(r => r[11])),
      sideMovement: getDiff(records.map(r => r[10])),
      sideTiltMovement: getDiff(records.map(r => r[9])),
    };
  }

  public createRecord(data: number[][], deviceType: DeviceType): RecordDeviceEntry {
    const extremeValues = getExtremeValues(data);
    const movement = RecordService.analyzeMovement(data);
    return this.analyzeData(extremeValues, movement, deviceType);
  }

  private analyzeData(data: number[], movement: Movement, deviceType: DeviceType): RecordDeviceEntry | null {
    const diff20 = 320;
    const referencePressure = this.recordQuery.getReferencePressure(deviceType);
    const pressure = {
      frontLeft: pressureToKg(data[1], referencePressure.frontLeft, diff20),
      frontRight: pressureToKg(data[0], referencePressure.frontRight, diff20),
      heel: pressureToKg(data[2], referencePressure.heel, diff20),
    };

    const tilt = {
      frontalTilt: frontalTiltToDegree(data[6], deviceType),
      sideTilt: sideTiltToDegree(data[8]),
    };

    return {
      movingMode: MovingMode.all,
      movement,
      ...tilt,
      pressure,
    };
  }
}
