import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@datorama/akita-ng-effects';
import {filter, map, tap, throttleTime} from 'rxjs/operators';
import {NotificationService} from '../../services/notification/notification.service';
import {getPositions} from '../../services/results/results.utils';
import {currentViolationsKey} from '../../services/storage/storage.const';
import {StorageService} from '../../services/storage/storage.service';
import {RecordQuery} from '../../store/record/record.query';
import {RecordStore} from '../../store/record/record.store';
import {SettingsQuery} from '../../store/settings/settings.query';
import {modeChangeAnalyzeAction, postureAnalyzeAction} from './notification.actions';

const minDelayBetweenNotifications = 60 * 1000;

@Injectable()
export class NotificationEffects {

  @Effect()
  postureAnalyze$ = this.actions$.pipe(ofType(postureAnalyzeAction),
    map(({records}) => this.notificationService.getBadPostures(records)),
    filter(messages => messages.length > 0),
    throttleTime(minDelayBetweenNotifications),
    tap(messages => this.recordStore.addViolations(messages)),
    tap(() => this.storageService.set(currentViolationsKey, this.recordQuery.getViolations())),
    filter(() => this.settingsQuery.getPostureAlertNotificationEnabled()),
    tap(messages => this.notificationService.showNotification('Posture Alert!', messages)),
  );

  @Effect()
  modeChangeAnalyze$ = this.actions$.pipe(ofType(modeChangeAnalyzeAction),
    map(({records}) => getPositions(records)),
    map(positions => this.notificationService.getPositionTooLong(positions)),
    filter(message => !!message),
    filter(() => this.settingsQuery.getPostureChangeNotificationEnabled()),
    tap(message => this.notificationService.showNotification('Change Posture!', [message]))
  );

  constructor(private actions$: Actions,
              private notificationService: NotificationService,
              private recordStore: RecordStore,
              private storageService: StorageService,
              private recordQuery: RecordQuery,
              private settingsQuery: SettingsQuery) {
  }
}
