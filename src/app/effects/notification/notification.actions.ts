import {createAction, props} from '@datorama/akita-ng-effects';
import {RecordEntry} from '../../store/record/record.model';

export const postureAnalyzeAction = createAction('[Notification] Posture analyze', props<{ records: RecordEntry[] }>());
export const modeChangeAnalyzeAction = createAction('[Notification] Mode change analyze', props<{ records: RecordEntry[] }>());
