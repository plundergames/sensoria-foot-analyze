import {createAction, props} from '@datorama/akita-ng-effects';
import {MovingMode} from '../../store/record/record.model';

export const createResultAction = createAction('[Result] Create result'
);

export const loadResultsOverviewAction = createAction('[Result] Load overview'
);

export const deleteResultAction = createAction('[Result] Delete result',
  props<{ id: string }>()
);

export const openResultAction = createAction('[Result] Open result',
  props<{
    id: string;
    movingMode: MovingMode;
  }>()
);

export const loadResultAction = createAction('[Result] Load result',
  props<{ id: string }>()
);
