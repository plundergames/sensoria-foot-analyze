import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@datorama/akita-ng-effects';
import {filter, map, mergeMap, pairwise, startWith, tap, withLatestFrom} from 'rxjs/operators';
import {EvaluationService} from '../../services/evaluation/evaluation.service';
import {ResultsService} from '../../services/results/results.service';
import {CompressService} from '../../services/compress/compress.service';
import {resultsOverviewKey} from '../../services/storage/storage.const';
import {StorageService} from '../../services/storage/storage.service';
import {RecordQuery} from '../../store/record/record.query';
import {Result} from '../../store/results/result.model';
import {ResultsQuery} from '../../store/results/results.query';
import {ResultsStore} from '../../store/results/results.store';
import {clearCurrentRecordsAction} from '../record/record.actions';
import {
  createResultAction,
  deleteResultAction,
  openResultAction,
  loadResultsOverviewAction,
  loadResultAction
} from './results.actions';

@Injectable()
export class ResultsEffects {

  @Effect({dispatch: true})
  createResult$ = this.actions$.pipe(ofType(createResultAction),
    withLatestFrom(this.recordQuery.recordState$),
    map(([_, recordState]) => this.resultsService.createResult(recordState)),
    tap(([resultsOverview]) => this.resultsStore.add(resultsOverview)),
    mergeMap(async ([resultOverview, result]) => {
      await this.storageService.add(resultsOverviewKey, resultOverview);
      const compressedRecords = this.compressService.compressEntries(result.records);
      await this.storageService.set(result.id, {...result, records: compressedRecords});
    }),
    map(() => clearCurrentRecordsAction)
  );

  @Effect()
  loadOverview$ = this.actions$.pipe(ofType(loadResultsOverviewAction),
    mergeMap(() => this.storageService.get(resultsOverviewKey)),
    tap(resultsOverview => this.resultsStore.set(resultsOverview))
  );

  @Effect()
  deleteResult$ = this.actions$.pipe(ofType(deleteResultAction),
    tap(({id}) => this.resultsStore.remove(id)),
    tap(({id}) => this.storageService.clear(id)),
    tap(({id}) => this.storageService.remove(resultsOverviewKey, id))
  );

  @Effect({dispatch: true})
  openResult$ = this.actions$.pipe(ofType(openResultAction),
    tap(({id, movingMode}) => this.resultsStore.sUpdate({selectedId: id, selectedMode: movingMode})),
    map(({id}) => id),
    startWith(null),
    pairwise(),
    filter(([prevId, nextId]) => prevId !== nextId),
    map(([_, id]) => loadResultAction({id})),
  );

  @Effect()
  loadResult$ = this.actions$.pipe(ofType(loadResultAction),
    mergeMap(({id}) => this.storageService.get(id)),
    map(result => ({
      ...result,
      records: this.compressService.decompressEntries(result.records)
    }) as Result),
    map(result => this.resultsStore.update({openResult: result}))
  );

  constructor(private actions$: Actions,
              private recordQuery: RecordQuery,
              private resultsService: ResultsService,
              private resultsStore: ResultsStore,
              private storageService: StorageService,
              private compressService: CompressService) {
  }

}
