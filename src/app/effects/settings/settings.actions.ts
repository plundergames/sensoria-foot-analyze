import {props} from '@datorama/akita-ng-effects';
import {createAction} from '@datorama/akita-ng-effects';

export const setPostureAlertNotificationsAction = createAction('[Settings] Set Posture Alert Notification',
  props<{ enable: boolean }>()
);

export const setPostureChangeNotificationsAction = createAction('[Settings] Set Posture Change Notification',
  props<{ enable: boolean }>()
);

export const saveSettingsAction = createAction('[Settings] Save settings');
export const loadSettingsAction = createAction('[Settings] Load settings');
