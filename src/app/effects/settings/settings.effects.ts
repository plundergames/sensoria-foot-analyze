import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@datorama/akita-ng-effects';
import {map, mergeMap, tap, withLatestFrom} from 'rxjs/operators';
import {settingsKey} from '../../services/storage/storage.const';
import {StorageService} from '../../services/storage/storage.service';
import {SettingsQuery} from '../../store/settings/settings.query';
import {SettingsStore} from '../../store/settings/settings.store';
import {
  loadSettingsAction,
  saveSettingsAction,
  setPostureAlertNotificationsAction,
  setPostureChangeNotificationsAction
} from './settings.actions';


@Injectable()
export class SettingsEffects {

  @Effect({dispatch: true})
  setPostureAlertNotificationEnabled$ = this.actions$.pipe(ofType(setPostureAlertNotificationsAction),
    tap(({enable}) => this.settingsStore.sUpdate({postureAlertNotificationEnabled: enable})),
    map(() => saveSettingsAction),
  );

  @Effect({dispatch: true})
  setPostureChangeNotificationEnabled$ = this.actions$.pipe(ofType(setPostureChangeNotificationsAction),
    tap(({enable}) => this.settingsStore.sUpdate({postureChangeNotificationEnabled: enable})),
    map(() => saveSettingsAction),
  );

  @Effect()
  saveSettings$ = this.actions$.pipe(ofType(saveSettingsAction),
    withLatestFrom(this.settingsQuery.settings$),
    tap(([_, settings]) => this.storageService.set(settingsKey, settings)),
  );

  @Effect()
  loadSettings$ = this.actions$.pipe(ofType(loadSettingsAction),
    mergeMap(() => this.storageService.get(settingsKey)),
    tap(settings => this.settingsStore.update(settings || {})),
  );

  constructor(private actions$: Actions,
              private settingsStore: SettingsStore,
              private settingsQuery: SettingsQuery,
              private storageService: StorageService) {
  }

}
