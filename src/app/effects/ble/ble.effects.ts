import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@datorama/akita-ng-effects';
import {from, of} from 'rxjs';
import {bufferTime, filter, map, mergeMap, tap} from 'rxjs/operators';
import {BleService} from '../../services/ble-service/ble.service';
import {deviceKey} from '../../services/storage/storage.const';
import {StorageService} from '../../services/storage/storage.service';
import {DeviceType} from '../../store/ble/ble.model';
import {BleQuery} from '../../store/ble/ble.query';
import {BleStore} from '../../store/ble/ble.store';
import {RecordQuery} from '../../store/record/record.query';
import {newRecordEntryAction} from '../record/record.actions';
import {
  clearSavedDeviceAction,
  connectAction,
  disconnectAction,
  emitLiveDataAction,
  stopScanningAction,
  tryConnectAction,
  tryConnectWhenSavedAction,
  tryLiveDataAction
} from './ble.actions';

export const dataInterval = 500;

@Injectable()
export class BleEffects {

  @Effect()
  tryConnect$ = this.actions$.pipe(
    ofType(tryConnectAction),
    tap(({deviceType}) => this.bleService.tryConnect(deviceType)));

  @Effect({dispatch: true})
  tryConnectWhenSaved$ = this.actions$.pipe(
    ofType(tryConnectWhenSavedAction),
    mergeMap(({deviceType}) =>
      from(
        this.storageService.get(deviceKey).then(devices => (devices || {})[deviceType])
      ).pipe(
        filter(storedDevice => !!storedDevice),
        map(() => deviceType)
      )
    ),
    map(deviceType => tryConnectAction({deviceType}))
  );

  @Effect({dispatch: true})
  connect$ = this.actions$.pipe(
    ofType(connectAction),
    tap(({device, deviceType}) => this.bleService.connect(device, deviceType)),
    map(() => stopScanningAction)
  );

  @Effect()
  tryLiveData$ = this.actions$.pipe(
    ofType(tryLiveDataAction),
    filter(() => this.recordQuery.isRecording()),
    mergeMap(() => of(...Object.entries(this.bleQuery.connectedDevices()))),
    tap(([deviceType, device]) => this.bleService.startLiveData(deviceType as DeviceType, device?.id))
  );

  @Effect({dispatch: true})
  disconnect$ = this.actions$.pipe(
    ofType(disconnectAction),
    mergeMap(async ({deviceType}) => {
      const id = this.bleQuery.connectedDevice(deviceType)?.id
        || this.bleQuery.waitingForConnect(deviceType)?.id;
      await this.bleService.disconnect(id);
      return deviceType;
    }),
    tap(deviceType => this.bleService.onDisconnect(deviceType)),
    map(deviceType => clearSavedDeviceAction({deviceType})),
  );

  @Effect()
  clearSavedDevice$ = this.actions$.pipe(
    ofType(clearSavedDeviceAction),
    mergeMap(({deviceType}) => this.storageService.clearProperty(deviceKey, deviceType))
  );

  @Effect()
  stopScanning$ = this.actions$.pipe(
    ofType(stopScanningAction),
    tap(() => this.bleService.stopScanning()),
  );

  @Effect({dispatch: true})
  emitLiveData$ = this.actions$.pipe(
    ofType(emitLiveDataAction),
    map(({liveData}) => liveData),
    filter(liveData => liveData?.data?.length > 0),
    tap(liveData => this.bleStore.sUpdate({liveData})),
    bufferTime(dataInterval),
    filter(records => records.length > 0),
    map(bufferedLiveData => newRecordEntryAction({data: bufferedLiveData}))
  );

  constructor(private actions$: Actions,
              private bleService: BleService,
              private bleQuery: BleQuery,
              private recordQuery: RecordQuery,
              private storageService: StorageService,
              private bleStore: BleStore) {
  }

}
