import {createAction, props} from '@datorama/akita-ng-effects';
import {Device, DeviceType, LiveData} from '../../store/ble/ble.model';

export const tryConnectAction = createAction('[Ble] Try connect',
  props<{ deviceType: DeviceType }>()
);

export const tryConnectWhenSavedAction = createAction('[Ble] Try connect when saved',
  props<{ deviceType: DeviceType }>()
);
export const tryLiveDataAction = createAction('[Ble] Try live data');
export const disconnectAction = createAction('[Ble] Disconnect',
  props<{ deviceType: DeviceType }>()
);

export const connectAction = createAction('[Ble] Connect',
  props<{
    deviceType: DeviceType;
    device: Device;
  }>()
);

export const stopScanningAction = createAction('[Ble] Stop scanning');
export const emitLiveDataAction = createAction('[Ble] Emit live data', props<{
  liveData: LiveData;
}>());

export const clearSavedDeviceAction = createAction('[Ble] Clear saved device',
  props<{ deviceType: DeviceType }>());
