import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@datorama/akita-ng-effects';
import {of} from 'rxjs';
import {filter, first, map, mergeMap, switchMap, tap, throttleTime} from 'rxjs/operators';
import {BleService} from '../../services/ble-service/ble.service';
import {CompressService} from '../../services/compress/compress.service';
import {RecordService} from '../../services/record/record.service';
import {currentViolationsKey, recordsKey, startDateKey} from '../../services/storage/storage.const';
import {StorageService} from '../../services/storage/storage.service';
import {DeviceType} from '../../store/ble/ble.model';
import {BleQuery} from '../../store/ble/ble.query';
import {Pressure, RecordEntry} from '../../store/record/record.model';
import {RecordQuery} from '../../store/record/record.query';
import {RecordStore} from '../../store/record/record.store';
import {tryLiveDataAction} from '../ble/ble.actions';
import {createResultAction} from '../results/results.actions';
import {
  clearCurrentRecordsAction,
  newRecordEntryAction,
  readCurrentRecordsAction,
  saveCurrentRecordsAction,
  setReferencePressureActions,
  startRecordingAction,
  stopRecordingAction
} from './record.actions';

@Injectable()
export class RecordEffects {

  @Effect()
  saveRecords$ = this.actions$.pipe(
    ofType(saveCurrentRecordsAction),
    filter(({records}) => records.length > 0),
    throttleTime(60 * 1000), // save every 60s
    map(({records}) => this.compressService.compressEntries(records)),
    tap(compressedRecords => this.storageService.set(recordsKey, compressedRecords))
  );

  @Effect({dispatch: true})
  readRecords$ = this.actions$.pipe(
    ofType(readCurrentRecordsAction),
    mergeMap(() => this.storageService.get(recordsKey)),
    filter(records => records?.length > 0),
    map(compressedRecords => this.compressService.decompressEntries(compressedRecords)),
    tap(records => this.recordStore.sUpdate({records})),
    switchMap(async () => {
      const violations = await this.storageService.get(currentViolationsKey);
      const startDate = await this.storageService.get(startDateKey);
      return [startDate, violations];
    }),
    map(([startDate, violations]) => startRecordingAction({startDate, violations})),
  );

  @Effect()
  clearRecords$ = this.actions$.pipe(
    ofType(clearCurrentRecordsAction),
    tap(() => this.storageService.clear(recordsKey)),
    tap(() => this.recordStore.sUpdate({records: [], startDate: null, violations: {}})),
  );

  @Effect({dispatch: true})
  startRecording$ = this.actions$.pipe(
    ofType(startRecordingAction),
    map(({startDate, violations}) => ({
      startDate: startDate || new Date(),
      violations: violations || {}
    })),
    tap(({startDate, violations}) => this.recordStore.sUpdate({isRecording: true, startDate, violations})),
    tap(({startDate}) => this.storageService.set(startDateKey, startDate)),
    map(() => tryLiveDataAction),
  );

  @Effect({dispatch: true})
  stopRecording$ = this.actions$.pipe(
    ofType(stopRecordingAction),
    tap(() => this.recordStore.sUpdate({isRecording: false})),
    map(() => Object.values(this.bleQuery.connectedDevices())),
    mergeMap(connectedDevices => Promise.all(
      connectedDevices.map(device => this.bleService.stopLiveData(device?.id)))
    ),
    map(() => createResultAction),
  );

  @Effect()
  newRecordEntry$ = this.actions$.pipe(
    ofType(newRecordEntryAction),
    filter(() => this.recordQuery.isRecording()),
    filter(() => this.recordQuery.isCalibrated()),
    map(({data}) => data.reduce((acc, item) => ({
        ...acc,
        [item.deviceType]: [...(acc[item.deviceType] || []), item.data]
      }),
      {} as Partial<Record<DeviceType, number[][]>>)),
    map(data => Object.entries(data) as [DeviceType, number[][]] []),
    map(data => data.reduce((acc, [deviceType, itemData]) => ({
      ...acc,
      [deviceType]: this.recordService.createRecord(itemData, deviceType)
    }), {} as RecordEntry)),
    map(record => RecordService.setMovingMode(record)),
    tap(record => this.recordStore.update(store => ({...store, records: [...store.records, record]})))
  );

  @Effect()
  setReferencePressure$ = this.actions$.pipe(
    ofType(setReferencePressureActions),
    switchMap(() => of(...Object.entries(this.bleQuery.connectedDevices())
      .filter(([_, value]) => value)
      .map(([key]) => key) as DeviceType[])),
    mergeMap(deviceType =>
      this.bleQuery.liveData$.pipe(
        filter(liveData => liveData?.deviceType === deviceType
          && liveData?.data?.length > 0),
        map(({data}) => ({
          heel: data[2],
          frontRight: data[0],
          frontLeft: data[1],
        } as Pressure)),
        map(pressure => [deviceType, pressure] as [DeviceType, Pressure]),
        first())),
    tap(([deviceType, pressure]) => this.recordStore.update(store => ({
      ...store,
      referencePressure: {
        ...store.referencePressure,
        [deviceType]: pressure
      }
    })))
  );

  constructor(private actions$: Actions,
              private storageService: StorageService,
              private recordStore: RecordStore,
              private bleService: BleService,
              private recordService: RecordService,
              private recordQuery: RecordQuery,
              private compressService: CompressService,
              private bleQuery: BleQuery) {
  }


}
