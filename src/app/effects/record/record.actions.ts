import {createAction, props} from '@datorama/akita-ng-effects';
import {LiveData} from '../../store/ble/ble.model';
import {RecordEntry} from '../../store/record/record.model';

export const newRecordEntryAction = createAction(
  '[Records] New Record Entry',
  props<{ data: LiveData[] }>(),
);
export const saveCurrentRecordsAction = createAction(
  '[Records] Save Records',
  props<{ records: RecordEntry[] }>()
);
export const clearCurrentRecordsAction = createAction(
  '[Records] Clear Records'
);
export const readCurrentRecordsAction = createAction(
  '[Records] Read Records'
);
export const startRecordingAction = createAction(
  '[Records] Start Recording',
  props<{
    startDate: Date | undefined | null;
    violations: Record<string, number>;
  }>(),
);
export const stopRecordingAction = createAction(
  '[Records] Stop Recording'
);

export const setReferencePressureActions = createAction(
  '[Records] Set Reference Pressure'
);
