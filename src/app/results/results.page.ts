import {Component, OnInit} from '@angular/core';
import {Actions} from '@datorama/akita-ng-effects';
import {ModalController} from '@ionic/angular';
import {Observable} from 'rxjs';
import {ConfirmModalComponent} from '../components/confirm-modal/confirm-modal.component';
import {deleteResultAction} from '../effects/results/results.actions';
import {ResultOverview} from '../store/results/result.model';
import {ResultsQuery} from '../store/results/results.query';

@Component({
  selector: 'app-results',
  templateUrl: './results.page.html',
  styleUrls: ['./results.page.scss'],
})
export class ResultsPage implements OnInit {

  resultsOverview$: Observable<ResultOverview[]>;

  constructor(private resultsQuery: ResultsQuery,
              private modalController: ModalController,
              private actions$: Actions) {
    this.resultsOverview$ = resultsQuery.resultsOverview$;
  }

  ngOnInit() {
  }

  async deleteResult(id: string) {
    const modal = await this.modalController.create({
      component: ConfirmModalComponent,
      componentProps: {
        text: 'Are you sure that you want to delete this recording?',
      },
    });
    modal.onDidDismiss().then(({data}) => {
      if (data) {
        this.actions$.dispatch(deleteResultAction({id}));
      }
    });
    await modal.present();
  }

}
