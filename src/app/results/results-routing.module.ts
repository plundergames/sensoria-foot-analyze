import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ResultsDetailComponent} from './results-detail/results-detail.component';

import {ResultsPage} from './results.page';

export const resultsDetailRoute = 'detail';

const routes: Routes = [
  {
    path: '',
    component: ResultsPage
  },
  {
    component: ResultsDetailComponent,
    path: `${resultsDetailRoute}/:id/:mode`,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ResultsPageRoutingModule {
}
