import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Actions} from '@datorama/akita-ng-effects';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {resultsRoute} from '../../app-routing.module';
import {MovingMode} from '../../store/record/record.model';
import {resultsDetailRoute} from '../results-routing.module';

@Component({
  selector: 'app-detail-switch',
  templateUrl: './detail-switch.component.html',
  styleUrls: ['./detail-switch.component.scss']
})
export class DetailSwitchComponent implements OnInit {
  @Input()
  id: string;

  movingModeEnum = MovingMode;

  movingMode$: Observable<MovingMode>;

  constructor(private router: Router, private actions$: Actions, private route: ActivatedRoute) {
    this.movingMode$ = route.paramMap.pipe(map(params => {
      const mode = params.get('mode');
      return Number(mode && mode !== '' ? mode : -1);
    }));
  }

  ngOnInit(): void {
  }

  openDetailPage(id: string, movingMode: MovingMode) {
    this.router.navigate([resultsRoute, resultsDetailRoute, id, movingMode]);
  }

}
