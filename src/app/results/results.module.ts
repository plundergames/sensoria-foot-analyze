import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {MatIconModule} from '@angular/material/icon';

import {IonicModule} from '@ionic/angular';
import {NgPipesModule} from 'ngx-pipes';
import {ComponentsModule} from '../components/components.module';
import {RecordPageModule} from '../record/record.module';

import {ResultsPageRoutingModule} from './results-routing.module';

import {ResultsPage} from './results.page';
import {DatePipe} from './pipes/date.pipe';
import {DurationPipe} from './pipes/duration.pipe';
import {DistributionPipe} from './pipes/distribution.pipe';
import { ResultsDetailComponent } from './results-detail/results-detail.component';
import { DetailSwitchComponent } from './detail-switch/detail-switch.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ResultsPageRoutingModule,
    MatIconModule,
    NgPipesModule,
    RecordPageModule,
    ComponentsModule
  ],
  declarations: [ResultsPage, DatePipe, DurationPipe, DistributionPipe, ResultsDetailComponent, DetailSwitchComponent]
})
export class ResultsPageModule {
}
