import {Pipe, PipeTransform} from '@angular/core';
import {simpleRound} from '../../functions/util';
import {MovingMode} from '../../store/record/record.model';

interface ModeVisualization {
  color: string;
  icon: string;
  value: string;
}

@Pipe({
  name: 'distribution'
})
export class DistributionPipe implements PipeTransform {

  transform(distribution: Partial<Record<MovingMode, number>>): ModeVisualization[] {
    const totalEntries = Object.values(distribution).reduce((acc, item) => acc + item, 0);
    const relaxedKneelTogether = {
      [MovingMode.relaxed]: (distribution[MovingMode.relaxed] || 0) + (distribution[MovingMode.kneel] || 0),
      [MovingMode.kneel]: 0
    };
    return Object.entries({...distribution, ...relaxedKneelTogether})
      .filter(([key]) => Number(key) as MovingMode !== MovingMode.all)
      .filter(([_, value]) => value > 0)
      .map(([key, value]) => ({
        value: simpleRound(value / totalEntries * 100, 0) + '%',
        color: this.getColor(Number(key)),
        icon: this.getIcon(Number(key)),
      }));
  }

  getColor(mode: MovingMode): string {
    switch (mode) {
      case MovingMode.move:
        return 'success';
      case MovingMode.sit:
        return 'warning';
      case MovingMode.stand:
        return 'tertiary';
      case MovingMode.relaxed:
      case MovingMode.kneel:
        return 'secondary';
    }
  }

  getIcon(mode: MovingMode): string {
    switch (mode) {
      case MovingMode.move:
        return 'directions_run';
      case MovingMode.sit:
        return 'airline_seat_recline_normal';
      case MovingMode.stand:
        return 'accessibility_new';
      case MovingMode.relaxed:
      case MovingMode.kneel:
        return 'self_improvement';
    }
  }

}
