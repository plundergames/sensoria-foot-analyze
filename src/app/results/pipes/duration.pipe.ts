import {Pipe, PipeTransform} from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'duration'
})
export class DurationPipe implements PipeTransform {

  transform(startDate: Date, endDate: Date): string {
    const diff = moment(endDate).diff(moment(startDate));
    const hours = Math.floor(moment.duration(diff).asHours());
    const minutes = moment.utc(diff).format('mm');
    return `${hours}:${minutes}h`;
  }

}
