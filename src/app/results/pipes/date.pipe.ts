import {Pipe, PipeTransform} from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'date'
})
export class DatePipe implements PipeTransform {

  transform(value: Date): string {
    return moment(value).format('YYYY-MM-DD HH:mm');
  }

}
