import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Actions} from '@datorama/akita-ng-effects';
import {UntilDestroy, untilDestroyed} from '@ngneat/until-destroy';
import {combineLatest, Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {openResultAction} from '../../effects/results/results.actions';
import {MovingMode} from '../../store/record/record.model';
import {ResultDetail} from '../../store/results/result.model';
import {ResultsQuery} from '../../store/results/results.query';

@UntilDestroy()
@Component({
  selector: 'app-results-detail',
  templateUrl: './results-detail.component.html',
  styleUrls: ['./results-detail.component.scss']
})
export class ResultsDetailComponent implements OnInit {

  movingMode$: Observable<MovingMode>;

  id$: Observable<string>;

  resultData: ResultDetail;

  constructor(private route: ActivatedRoute, private resultQuery: ResultsQuery, private actions$: Actions) {
    this.movingMode$ = route.paramMap.pipe(map(params => Number(params.get('mode'))));
    this.id$ = route.paramMap.pipe(map(params => params.get('id')));
    combineLatest([this.movingMode$, this.id$])
      .pipe(untilDestroyed(this))
      .subscribe(([movingMode, id]) => this.actions$.dispatch(openResultAction({id, movingMode})));
    this.resultQuery.openDetail$.pipe(untilDestroyed(this)).subscribe(openResult => this.resultData = openResult);
  }

  ngOnInit(): void {
  }

}
