import {Component} from '@angular/core';
import {Actions} from '@datorama/akita-ng-effects';
import {Platform} from '@ionic/angular';
import {Observable} from 'rxjs';
import {tryConnectWhenSavedAction} from './effects/ble/ble.actions';
import {readCurrentRecordsAction} from './effects/record/record.actions';
import {loadResultsOverviewAction} from './effects/results/results.actions';
import {loadSettingsAction} from './effects/settings/settings.actions';
import {DeviceType} from './store/ble/ble.model';
import {BleQuery} from './store/ble/ble.query';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  scanningType$: Observable<DeviceType>;

  constructor(private actions$: Actions, private platform: Platform, private bleQuery: BleQuery) {
    this.scanningType$ = bleQuery.scanningType$;
    platform.ready().then(() => {
      this.setup();
    });
  }

  private setup() {
    this.actions$.dispatch(readCurrentRecordsAction);
    this.actions$.dispatch(loadResultsOverviewAction);
    this.actions$.dispatch(loadSettingsAction);
    this.actions$.dispatch(tryConnectWhenSavedAction({deviceType: DeviceType.right}));
    this.actions$.dispatch(tryConnectWhenSavedAction({deviceType: DeviceType.left}));
  }
}
