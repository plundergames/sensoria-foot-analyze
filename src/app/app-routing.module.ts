import {NgModule} from '@angular/core';
import {PreloadAllModules, RouterModule, Routes} from '@angular/router';
import {ComponentsModule} from './components/components.module';

export const recordRoute = 'record';
export const resultsRoute = 'results';
export const debugRoute = 'debug';

const routes: Routes = [
  {
    path: debugRoute,
    loadChildren: () => import('./debug/debug.module').then(m => m.DebugPageModule)
  },
  {
    path: '',
    redirectTo: recordRoute,
    pathMatch: 'full'
  },
  {
    path: recordRoute,
    loadChildren: () => import('./record/record.module').then(m => m.RecordPageModule)
  },
  {
    path: resultsRoute,
    loadChildren: () => import('./results/results.module').then(m => m.ResultsPageModule)
  },

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {preloadingStrategy: PreloadAllModules}),
    ComponentsModule
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
