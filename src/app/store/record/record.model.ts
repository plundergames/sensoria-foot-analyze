import {DeviceType} from '../ble/ble.model';

export interface RecordState {
  isRecording: boolean;
  records: RecordEntry[];
  startDate: Date;
  violations: Record<string, number>;
  referencePressure: Partial<Record<DeviceType, Pressure>> | null;
}

export interface Pressure {
  frontLeft: number; // kg
  frontRight: number; // kg
  heel: number; // kg
}

export enum MovingMode {
  all,
  sit,
  stand,
  move,
  relaxed,
  kneel,
}

export interface Movement {
  frontalMovement: number;
  sideMovement: number;
  sideTiltMovement: number;
}

export type RecordEntry = Partial<Record<DeviceType, RecordDeviceEntry>>;

export interface RecordDeviceEntry {
  movingMode: MovingMode;
  movement: Movement;
  frontalTilt: number; // degree
  sideTilt: number; // degree
  pressure: Pressure;
}
