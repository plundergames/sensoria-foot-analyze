import {Injectable} from '@angular/core';
import {Query} from '@datorama/akita';
import {Actions} from '@datorama/akita-ng-effects';
import {UntilDestroy, untilDestroyed} from '@ngneat/until-destroy';
import {filter, map, throttleTime} from 'rxjs/operators';
import {modeChangeAnalyzeAction, postureAnalyzeAction} from '../../effects/notification/notification.actions';
import {saveCurrentRecordsAction} from '../../effects/record/record.actions';
import {DeviceType} from '../ble/ble.model';
import {Pressure, RecordState} from './record.model';
import {RecordStore} from './record.store';

export const postureCheckInterval = 15;

@UntilDestroy()
@Injectable({providedIn: 'root'})
export class RecordQuery extends Query<RecordState> {

  isRecording$ = this.select('isRecording');

  lastRecord$ = this.select(({records}) => records[records.length - 1]);

  recordState$ = this.select();

  isCalibrated$ = this.select('referencePressure').pipe(map(referencePressure => !!referencePressure));

  constructor(protected store: RecordStore, private actions$: Actions) {
    super(store);
    this.select('records')
      .pipe(
        untilDestroyed(this),
      ).subscribe(records => actions$.dispatch(saveCurrentRecordsAction({records})));

    this.select(({records}) => records.slice(-postureCheckInterval))
      .pipe(
        filter(records => records.length >= postureCheckInterval),
        untilDestroyed(this)
      ).subscribe(records => this.actions$.dispatch(postureAnalyzeAction({records})));

    this.select('records').pipe(
      throttleTime(10 * 60 * 1000), // 10 min
      untilDestroyed(this)
    ).subscribe(records => this.actions$.dispatch(modeChangeAnalyzeAction({records})));
  }

  isCalibrated(): boolean {
    return this.getValue().referencePressure != null;
  }

  isRecording(): boolean {
    return this.getValue().isRecording;
  }

  getViolations(): Record<string, number> {
    return this.getValue().violations;
  }

  getReferencePressure(deviceType: DeviceType): Pressure {
    return this.getValue().referencePressure[deviceType];
  }

}
