import {Injectable} from '@angular/core';
import {Store, StoreConfig} from '@datorama/akita';
import {RecordState} from './record.model';

export function createInitialState(): RecordState {
  return {
    isRecording: false,
    records: [],
    startDate: null,
    violations: {},
    referencePressure: null,
  };
}

@Injectable({providedIn: 'root'})
@StoreConfig({name: 'record'})
export class RecordStore extends Store<RecordState> {

  constructor() {
    super(createInitialState());
  }

  sUpdate(partialUpdate: Partial<RecordState>) {
    this.update(store => ({...store, ...partialUpdate}));
  }

  addViolations(messages: string[]) {
    this.update(store => ({
      ...store,
      violations: messages.reduce((acc, item) => ({
        ...acc,
        [item]: (acc[item] || 0) + 1,
      }), store.violations || {})
    }));
  }

}
