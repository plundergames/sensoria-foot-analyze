import {Injectable} from '@angular/core';
import {Store, StoreConfig} from '@datorama/akita';
import {BleState} from './ble.model';

export function createInitialState(): BleState {
  return {
    liveData: null,
    debugNotifications: null,
    bluetoothEnabled: true,
    connectedDevices: {},
    scanningType: null,
    scannedDevices: [],
    waitingForConnect: {},
  };
}

@Injectable({providedIn: 'root'})
@StoreConfig({name: 'ble'})
export class BleStore extends Store<BleState> {

  constructor() {
    super(createInitialState());
  }

  sUpdate(partialUpdate: Partial<BleState>) {
    this.update(store => ({...store, ...partialUpdate}));
  }

}
