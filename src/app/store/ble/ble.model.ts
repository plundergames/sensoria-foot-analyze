export interface BleState {
  liveData: LiveData;
  debugNotifications: any;
  bluetoothEnabled: boolean;
  connectedDevices: Partial<Record<DeviceType, Device>>;
  scanningType: DeviceType | null;
  scannedDevices: any[];
  waitingForConnect: Partial<Record<DeviceType, null | Device>>;
}

export enum DeviceType {
  right = 'right',
  left = 'left',
}

export interface Device {
  id: string;
  characteristics: any;
}

export interface LiveData {
  deviceType: DeviceType;
  data: number[];
}


