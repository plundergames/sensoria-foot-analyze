import {Injectable} from '@angular/core';
import {Query} from '@datorama/akita';
import {UntilDestroy} from '@ngneat/until-destroy';
import {map} from 'rxjs/operators';
import {BleState, Device, DeviceType} from './ble.model';
import {BleStore} from './ble.store';

@UntilDestroy()
@Injectable({providedIn: 'root'})
export class BleQuery extends Query<BleState> {

  public bluetoothEnabled$ = this.select(store => store.bluetoothEnabled);
  public connectedDevices$ = this.select(store => store.connectedDevices);
  public isConnected$ = this.connectedDevices$.pipe(map(devices =>
    Object.values(devices).filter(device => device).length > 0));
  public scanningType$ = this.select(store => store.scanningType);
  public scannedDevices$ = this.select(store => store.scannedDevices);
  public liveData$ = this.select('liveData');
  public waitingForConnect$ = this.select('waitingForConnect');

  constructor(protected store: BleStore) {
    super(store);
  }

  public scanningType(): DeviceType {
    return this.getValue().scanningType;
  }

  public connectedDevice(deviceType: DeviceType): Device | null {
    return (this.getValue().connectedDevices || {})[deviceType];
  }

  public connectedDevices(): Partial<Record<DeviceType, Device>> {
    return this.getValue().connectedDevices;
  }

  public waitingForConnect(deviceType: DeviceType): Device {
    return this.getValue().waitingForConnect[deviceType];
  }

}
