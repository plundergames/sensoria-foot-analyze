export interface SettingsState {
  postureAlertNotificationEnabled: boolean;
  postureChangeNotificationEnabled: boolean;
}
