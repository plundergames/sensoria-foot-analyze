import {Injectable} from '@angular/core';
import {Store, StoreConfig} from '@datorama/akita';
import {SettingsState} from './setting.model';

export function createInitialState(): SettingsState {
  return {
    postureAlertNotificationEnabled: false,
    postureChangeNotificationEnabled: false,
  };
}

@Injectable({providedIn: 'root'})
@StoreConfig({name: 'settings'})
export class SettingsStore extends Store<SettingsState> {

  constructor() {
    super(createInitialState());
  }

  sUpdate(partialUpdate: Partial<SettingsState>) {
    this.update(store => ({...store, ...partialUpdate}));
  }

}
