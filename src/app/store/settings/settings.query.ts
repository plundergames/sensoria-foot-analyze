import {Injectable} from '@angular/core';
import {Query} from '@datorama/akita';
import {SettingsState} from './setting.model';
import {SettingsStore} from './settings.store';

@Injectable({providedIn: 'root'})
export class SettingsQuery extends Query<SettingsState> {

  postureAlertNotificationEnabled$ = this.select('postureAlertNotificationEnabled');
  postureChangeNotificationEnabled$ = this.select('postureChangeNotificationEnabled');
  settings$ = this.select();

  constructor(protected store: SettingsStore) {
    super(store);
  }

  getPostureAlertNotificationEnabled(): boolean {
    return this.getValue().postureAlertNotificationEnabled;
  }

  getPostureChangeNotificationEnabled(): boolean {
    return this.getValue().postureChangeNotificationEnabled;
  }

}
