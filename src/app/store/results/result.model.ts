import {EntityState} from '@datorama/akita';
import {Distribution, Histogram} from '../../services/histogram/histogram.model';
import {TimeData} from '../../services/time-data/time-data.model';
import {MovingMode, RecordEntry} from '../record/record.model';

export interface ResultsState extends EntityState<ResultOverview> {
  openResult: Result;
  selectedMode: MovingMode;
  selectedId: string;
}

export interface ResultBase {
  id: string;
  startDate: Date;
  endDate: Date;
}

export interface ResultOverview extends ResultBase {
  violations: number;
  distribution: Partial<Record<MovingMode, number>>;
}

export interface Result extends ResultBase {
  records: RecordEntry[];
}

export interface ResultDetail extends ResultBase {
  totalRecords: number;
  frontalTilt: Histogram;
  sideTilt: Histogram;
  sideMovement: Histogram;
  sideTiltMovement: Histogram;
  pressure: Distribution;
  totalPressure: Histogram;
  pressureOverTime: TimeData;
}
