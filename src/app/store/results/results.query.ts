import {Injectable} from '@angular/core';
import {QueryEntity} from '@datorama/akita';
import {combineLatest} from 'rxjs';
import {filter, map} from 'rxjs/operators';
import {EvaluationService} from '../../services/evaluation/evaluation.service';
import {ResultsState} from './result.model';
import {ResultsStore} from './results.store';

@Injectable({providedIn: 'root'})
export class ResultsQuery extends QueryEntity<ResultsState> {

  resultsOverview$ = this.selectAll();
  openDetail$ = combineLatest([
    this.select('openResult'),
    this.select('selectedMode')
  ]).pipe(
    filter(([result, movingMode]) => !!result && movingMode !== null),
    map(([result, movingMode]) => this.evaluationService.evaluateResult(result, movingMode))
  );

  constructor(protected store: ResultsStore, private evaluationService: EvaluationService) {
    super(store);
  }

}
