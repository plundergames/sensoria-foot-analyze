import {Injectable} from '@angular/core';
import {EntityStore, StoreConfig} from '@datorama/akita';
import {ResultsState} from './result.model';

@Injectable({providedIn: 'root'})
@StoreConfig({name: 'results'})
export class ResultsStore extends EntityStore<ResultsState> {

  constructor() {
    super();
  }

  sUpdate(partialUpdate: Partial<ResultsState>) {
    this.update(store => ({...store, ...partialUpdate}));
  }

}
