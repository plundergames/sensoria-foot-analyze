import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { DebugPage } from './debug.page';

import { DebugPageRoutingModule } from './debug-routing.module';
import { DisplayNotificationPipe } from './pipes/display-notification.pipe';
import { DisplayRangePipe } from './pipes/display-range.pipe';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DebugPageRoutingModule
  ],
  declarations: [DebugPage, DisplayNotificationPipe, DisplayRangePipe]
})
export class DebugPageModule {}
