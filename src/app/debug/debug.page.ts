import {Component, OnDestroy} from '@angular/core';
import {UntilDestroy} from '@ngneat/until-destroy';
import {Observable} from 'rxjs';
import {filter, map, throttleTime} from 'rxjs/operators';
import {BleDebugService} from '../services/ble-service/ble-debug.service';
import {BleService} from '../services/ble-service/ble.service';
import {Device, DeviceType} from '../store/ble/ble.model';
import {BleQuery} from '../store/ble/ble.query';

@UntilDestroy()
@Component({
  selector: 'app-debug',
  templateUrl: 'debug.page.html',
  styleUrls: ['debug.page.scss'],
})
export class DebugPage implements OnDestroy {

  isScanning$: Observable<boolean>;
  connectedDevice$: Observable<Partial<Record<DeviceType, Device>>>;
  scannedDevices$: Observable<any[]>;
  notifications$: Observable<any[]>;

  constructor(private bleService: BleService, private bleDebugService: BleDebugService, private bleQuery: BleQuery) {
    this.isScanning$ = this.bleQuery.scanningType$
      .pipe(filter(deviceType => deviceType === DeviceType.right),
        map(deviceType => !!deviceType)
      );
    this.connectedDevice$ = this.bleQuery.connectedDevices$;
    this.scannedDevices$ = this.bleQuery.scannedDevices$;
    this.notifications$ = this.bleDebugService.debugNotifications$
      .pipe(throttleTime(500));
    this.connectedDevice$.pipe(
      filter(device => !!device?.right || !!device?.left)
    ).subscribe(({right, left}) => this.bleDebugService.startDebugNotifications(right || left));
  }

  ngOnDestroy() {
    this.bleDebugService.stopDebugNotifications();
  }
}
