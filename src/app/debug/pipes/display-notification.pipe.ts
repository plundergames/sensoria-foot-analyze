import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'displayNotification'
})
export class DisplayNotificationPipe implements PipeTransform {

  transform(value: any[], ...args: unknown[]): unknown {
    return value
      .reduce((acc, item) => {
        const currentRow = acc[acc.length - 1];
        if (currentRow && currentRow.length < 10) {
          currentRow.push(item);
          return acc;
        } else {
          return [...acc, [item]];
        }
      }, []);
  }

}
