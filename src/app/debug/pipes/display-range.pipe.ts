import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'displayRange'
})
export class DisplayRangePipe implements PipeTransform {

  transform(value: any, ...args: unknown[]): string {
    const range = value.max - value.min;
    const maxRange = 1024;
    const reverseRange = (maxRange - range) * 256 / maxRange;
    return `background-color: rgb(255, ${reverseRange}, ${reverseRange})`;
  }

}
