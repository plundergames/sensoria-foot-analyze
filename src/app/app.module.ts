import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {RouteReuseStrategy} from '@angular/router';
import {AkitaNgEffectsModule} from '@datorama/akita-ng-effects';
import {AkitaNgDevtools} from '@datorama/akita-ngdevtools';
import {BLE} from '@ionic-native/ble/ngx';
import {LocalNotifications} from '@ionic-native/local-notifications/ngx';

import {IonicModule, IonicRouteStrategy} from '@ionic/angular';
import {IonicStorageModule} from '@ionic/storage-angular';
import {environment} from '../environments/environment';
import {AppRoutingModule} from './app-routing.module';

import {AppComponent} from './app.component';
import {ComponentsModule} from './components/components.module';
import {NotificationEffects} from './effects/notification/notification.effects';
import {BleEffects} from './effects/ble/ble.effects';
import {RecordEffects} from './effects/record/record.effects';
import {ResultsEffects} from './effects/results/results.effects';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {SettingsEffects} from './effects/settings/settings.effects';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    IonicStorageModule.forRoot(),
    ComponentsModule,
    AkitaNgEffectsModule.forRoot([RecordEffects, NotificationEffects, BleEffects, ResultsEffects, SettingsEffects]),
    environment.production ? [] : AkitaNgDevtools.forRoot(),
    BrowserAnimationsModule],
  providers: [
    {provide: RouteReuseStrategy, useClass: IonicRouteStrategy},
    BLE,
    LocalNotifications,

  ],
  bootstrap: [AppComponent],
})
export class AppModule {
}
