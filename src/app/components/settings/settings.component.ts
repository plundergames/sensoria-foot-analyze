import {Component, OnInit} from '@angular/core';
import {Actions} from '@datorama/akita-ng-effects';
import {ModalController} from '@ionic/angular';
import {
  setPostureAlertNotificationsAction,
  setPostureChangeNotificationsAction
} from '../../effects/settings/settings.actions';
import {SettingsQuery} from '../../store/settings/settings.query';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {

  postureAlertNotificationEnabled$ = this.settingsQuery.postureAlertNotificationEnabled$;
  postureChangeNotificationEnabled$ = this.settingsQuery.postureChangeNotificationEnabled$;

  constructor(private settingsQuery: SettingsQuery, private actions$: Actions, private modalController: ModalController) {
  }

  ngOnInit(): void {
  }

  setPostureAlertNotificationEnabled({detail: {checked}}) {
    this.actions$.dispatch(setPostureAlertNotificationsAction({enable: checked}));
  }

  setPostureChangeNotificationsEnabled({detail: {checked}}) {
    this.actions$.dispatch(setPostureChangeNotificationsAction({enable: checked}));
  }

  async closeSettings() {
    await this.modalController.dismiss();
  }
}
