import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {ChartDataSets, ChartOptions, ChartType} from 'chart.js';
import {Color, Label} from 'ng2-charts';
import {simpleRound} from '../../functions/util';
import {Distribution} from '../../services/histogram/histogram.model';

@Component({
  selector: 'app-distribution-chart',
  templateUrl: './distribution-chart.component.html',
  styleUrls: ['./distribution-chart.component.scss']
})
export class DistributionChartComponent implements OnInit, OnChanges {

  @Input()
  public distributionData: Distribution;

  @Input()
  public title: string;

  public barChartOptions: ChartOptions = {
    responsive: true,
    showLines: true,
    elements: {
      rectangle: {
        borderWidth: 3,
      }
    },
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: true,
          min: 0
        }
      }]
    },
  };
  public barChartLabels: Label[] = [];
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;
  public barChartPlugins = [];

  public barChartData: ChartDataSets[] = [];
  public chartColors: Color[] = [];

  constructor() {
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.distributionData) {
      const newDistributionData: Distribution = changes.distributionData.currentValue;
      const totalValue = newDistributionData
        .flatMap(([_, data]) => data)
        .reduce((acc, {count}) => count + acc, 0);
      this.barChartData = newDistributionData.map(([deviceType, data]) => ({
        label: deviceType,
        data: data.map(d => simpleRound(d.count / totalValue * 100, 1))
      }));
      this.barChartLabels = newDistributionData[0][1].map(({name}) => name);
    }
  }

}
