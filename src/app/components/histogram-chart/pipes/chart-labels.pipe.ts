import {Pipe, PipeTransform} from '@angular/core';
import {Label} from 'ng2-charts';
import {Histogram} from '../../../services/histogram/histogram.model';

@Pipe({
  name: 'chartLabels'
})
export class ChartLabelsPipe implements PipeTransform {

  transform([[_, dataSet]]: Histogram): Label[] {
    return dataSet.buckets.map(({min, max}) =>
      `${min !== null ? min : 'min'} to ${max !== null ? max : 'max'}`);
  }

}
