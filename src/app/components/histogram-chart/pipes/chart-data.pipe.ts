import {Pipe, PipeTransform} from '@angular/core';
import {ChartDataSets} from 'chart.js';
import {simpleRound} from '../../../functions/util';
import {Histogram} from '../../../services/histogram/histogram.model';

@Pipe({
  name: 'chartData'
})
export class ChartDataPipe implements PipeTransform {

  transform(histogramData: Histogram): ChartDataSets[] {
    return histogramData.map(([deviceType, dataSet]) => {
      const totalValue = dataSet.buckets.reduce((acc, item) => item.count + acc, 0);
      return {
        data: dataSet.buckets.map(bucket => simpleRound(bucket.count / totalValue * 100, 1)),
        label: deviceType
      };
    });
  }

}
