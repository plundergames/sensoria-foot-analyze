import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {IonicModule} from '@ionic/angular';
import {ChartsModule} from 'ng2-charts';
import {HeaderComponent} from './header/header.component';
import {TabBarComponent} from './tab-bar/tab-bar.component';
import {ConfirmModalComponent} from './confirm-modal/confirm-modal.component';
import { HistogramChartComponent } from './histogram-chart/histogram-chart.component';
import { ChartDataPipe } from './histogram-chart/pipes/chart-data.pipe';
import { ChartLabelsPipe } from './histogram-chart/pipes/chart-labels.pipe';
import { DistributionChartComponent } from './distribution-chart/distribution-chart.component';
import { ScanModalComponent } from './scan-modal/scan-modal.component';
import { SettingsComponent } from './settings/settings.component';
import { LineChartComponent } from './line-chart/line-chart.component';

@NgModule({
  declarations: [
    TabBarComponent,
    HeaderComponent,
    ConfirmModalComponent,
    HistogramChartComponent,
    ChartDataPipe,
    ChartLabelsPipe,
    DistributionChartComponent,
    ScanModalComponent,
    SettingsComponent,
    LineChartComponent
  ],
  exports: [
    TabBarComponent,
    HeaderComponent,
    ConfirmModalComponent,
    HistogramChartComponent,
    DistributionChartComponent,
    ScanModalComponent,
    LineChartComponent,
  ],
  imports: [IonicModule, CommonModule, ChartsModule],
})
export class ComponentsModule {
}
