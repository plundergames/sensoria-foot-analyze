import {Component, OnInit} from '@angular/core';
import {Actions} from '@datorama/akita-ng-effects';
import {Observable} from 'rxjs';
import {connectAction, stopScanningAction} from '../../effects/ble/ble.actions';
import {Device, DeviceType} from '../../store/ble/ble.model';
import {BleQuery} from '../../store/ble/ble.query';

@Component({
  selector: 'app-scan-modal',
  templateUrl: './scan-modal.component.html',
  styleUrls: ['./scan-modal.component.scss']
})
export class ScanModalComponent implements OnInit {

  scanningType$: Observable<DeviceType>;

  scannedDevices$: Observable<any>;

  constructor(private bleQuery: BleQuery, private actions$: Actions) {
    this.scannedDevices$ = bleQuery.scannedDevices$;
    this.scanningType$ = bleQuery.scanningType$;
  }

  ngOnInit(): void {
  }

  stopScanning() {
    this.actions$.dispatch(stopScanningAction);
  }

  connect(device: Device) {
    this.actions$.dispatch(connectAction({device, deviceType: this.bleQuery.scanningType()}));
  }
}
