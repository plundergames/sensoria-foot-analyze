import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {ChartDataSets, ChartOptions, ChartType} from 'chart.js';
import * as moment from 'moment';
import {Color, Label} from 'ng2-charts';
import {TimeData} from '../../services/time-data/time-data.model';

@Component({
  selector: 'app-line-chart',
  templateUrl: './line-chart.component.html',
  styleUrls: ['./line-chart.component.scss']
})
export class LineChartComponent implements OnInit, OnChanges {

  @Input()
  title: string;

  @Input()
  timeData: TimeData;

  public lineChartData: ChartDataSets[] = [];
  public lineChartLabels: Label[] = [];
  public lineChartOptions: ChartOptions = {
    responsive: true,
  };
  public lineChartColors: Color[] = [];
  public lineChartLegend = true;
  public lineChartType: ChartType = 'line';
  public lineChartPlugins = [];

  constructor() {
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.timeData) {
      const newTimeData: TimeData = changes.timeData.currentValue;
      this.lineChartData = newTimeData.data.map(([deviceType, data]) => ({
        label: deviceType,
        data,
      }));
      this.lineChartLabels = newTimeData.timeEntries.map(date => moment(date).format('HH:mm'));
    }
  }

}
