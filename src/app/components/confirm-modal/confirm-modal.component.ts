import {Component, Input, OnInit} from '@angular/core';
import {ModalController} from '@ionic/angular';

@Component({
  selector: 'app-confirm-modal',
  templateUrl: './confirm-modal.component.html',
  styleUrls: ['./confirm-modal.component.scss']
})
export class ConfirmModalComponent implements OnInit {

  @Input()
  text: string;

  constructor(private modalController: ModalController) {
  }

  ngOnInit(): void {
  }

  close(value: boolean) {
    this.modalController.dismiss(value);
  }

}
