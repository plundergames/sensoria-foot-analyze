import {Location} from '@angular/common';
import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {Actions} from '@datorama/akita-ng-effects';
import {ModalController} from '@ionic/angular';
import {combineLatest, Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {disconnectAction, stopScanningAction, tryConnectAction} from '../../effects/ble/ble.actions';
import {DeviceType} from '../../store/ble/ble.model';
import {BleQuery} from '../../store/ble/ble.query';
import {SettingsComponent} from '../settings/settings.component';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent {

  deviceTypeEnum = DeviceType;

  isBluetoothEnabled$: Observable<boolean>;
  scanningType$: Observable<DeviceType>;
  connectedDevices: Observable<any>;
  showBackButton: boolean;
  urlParts: string[];
  searchingMap$: Observable<Partial<Record<DeviceType, boolean>>>;

  constructor(private bleQuery: BleQuery,
              private actions$: Actions,
              private location: Location,
              private router: Router,
              private modalController: ModalController) {
    this.isBluetoothEnabled$ = this.bleQuery.bluetoothEnabled$;
    this.scanningType$ = this.bleQuery.scanningType$;
    this.connectedDevices = this.bleQuery.connectedDevices$;
    this.searchingMap$ = combineLatest([this.bleQuery.waitingForConnect$, this.bleQuery.scanningType$])
      .pipe(map(([waiting, scanning]) => Object.entries(waiting).reduce((acc, [key, value]) => {
        acc[key] = !!value || scanning === key;
        return acc;
      }, {} as Partial<Record<DeviceType, boolean>>)));

    location.onUrlChange(url => {
      this.urlParts = url.split('/').filter(part => part !== '');
      this.showBackButton = this.urlParts.length > 1;
    });
  }

  connectButtonClick(deviceType: DeviceType) {
    if (this.bleQuery.connectedDevice(deviceType) || this.bleQuery.waitingForConnect(deviceType)) {
      this.actions$.dispatch(disconnectAction({deviceType}));
    } else if (this.bleQuery.scanningType() === deviceType) {
      this.actions$.dispatch(stopScanningAction());
    } else {
      this.actions$.dispatch(tryConnectAction({deviceType}));
    }
  }

  navigateBack() {
    this.router.navigate([this.urlParts[0]]);
  }

  async openSettings() {
    const modal = await this.modalController.create({
      component: SettingsComponent,
    });
    await modal.present();
  }
}
